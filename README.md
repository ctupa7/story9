# Story-9 PPW

Repository ini merupakan hasil pengerjaan story-9 untuk mata kuliah Perancangan & Pemrograman Web.

## Heroku

Klik [disini](https://ctupa7-story9.herokuapp.com/) untuk melihat website.

## Pipeline and Coverage

[![pipeline status](https://gitlab.com/ctupa7/story9/badges/master/pipeline.svg)](https://gitlab.com/ctupa7/story9/commits/master)
[![coverage report](https://gitlab.com/ctupa7/story9/badges/master/coverage.svg)](https://gitlab.com/ctupa7/story9/commits/master)