from django.test import TestCase, Client
from django.urls import resolve
from app_auth import views
from auth.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from django.contrib.auth.models import User
import os
import time

c = Client()

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_auth_benar(self):
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_url_slash_login_benar(self):
        response = c.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_url_slash_logout_benar(self):
        response = c.get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_tidak_ada_url_auth(self):
        response = c.get('/auth/')
        self.assertEqual(response.status_code, 404)

    def test_apakah_pake_fungsi_register(self):
        response = resolve('/register/')
        self.assertEqual(response.func, views.register)

    def test_apakah_pake_fungsi_profile(self):
        response = resolve('/profile/')
        self.assertEqual(response.func, views.profile)

    def test_apakah_pake_template_register_html(self):
        response = c.get('/register/')
        self.assertTemplateUsed(response, 'app_auth/register.html')

    def test_apakah_pake_template_login_html(self):
        response = c.get('/login/')
        self.assertTemplateUsed(response, 'app_auth/login.html')

    def test_apakah_pake_template_logout_html(self):
        response = c.get('/logout/')
        self.assertTemplateUsed(response, 'app_auth/logout.html')

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        User.objects.create_user('coba', '' , 'coba')
        super(FuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTest, self).tearDown()